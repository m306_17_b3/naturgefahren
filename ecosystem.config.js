module.exports = {
    /**
     * Application configuration section
     * http://pm2.keymetrics.io/docs/usage/application-declaration/
     */
    apps: [{
        name: 'naturgefahren',
        script: 'app.js',
        env: {},
        env_production: {
            NODE_ENV: 'production'
        }
    }, ],

    /**
     * Deployment section
     * http://pm2.keymetrics.io/docs/usage/deployment/
     */
    deploy: {
        prod: {
            user: 'alarm',
            host: 'digitalschweiz.ch',
            ref: 'origin/master',
            repo: '/srv/git/naturgefahren.git',
            path: '/var/www-edu/naturgefahren/prod',
            'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production'
        }
    }
};