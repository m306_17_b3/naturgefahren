#!/usr/bin/python
# http://gdal.org/python/
from osgeo import ogr
dataSource = ogr.Open("./detailabkl_punkte.shp")
geojson = ""
#print(dataSource.GetLayerCount())
#print(dataSource.ExportToJson())

def dump(obj):
  for attr in dir(obj):
    print("obj.%s = %s" % (attr, getattr(obj, attr)))

for i in range(0, dataSource.GetLayerCount()):
#    print(i);
    layer = dataSource.GetLayer(i)
#    print(layer.GetFeatureCount())
    for k in range(0, layer.GetFeatureCount()):
#        print(k)
#        dump(layer)
        feature = layer.GetFeature(k)
        geojson += feature.ExportToJson() + ","
print(geojson)