#!/usr/bin/env bash

echo "[+] starting mkdist [jtools]..."

DIST_DIR=../dist/jtools

echo "[*] updating files list in git..."
git add .

echo "[*] committing files..."
git commit -a -m "automated commit from mkdist.sh"

echo "[*] making sure DIST_DIR ($DIST_DIR) exist..."
mkdir -p $DIST_DIR

echo "[*] deleting contents..."
rm -r $DIST_DIR/*

echo "[*] copying files under git version control (via git ls-files) into directory..."
cp $(git ls-files) $DIST_DIR

echo "[+] finished mkdist [jtools]!"
