const TAG = 'jvalidator';
const VERSION = '0.1.2';

const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: `${TAG}V${VERSION}`, level: 'debug' });

const ObjectId = require('mongoose').Types.ObjectId;
const _ = require('lodash');
const util = require('util');
const emailValidator = require("email-validator");

// TypeError, ReferenceError, RangeError -> 500
const errorStatusCodes = {
    'TokenExpiredError': 401,

    'UserNotFoundError': 422,
    'RequiredError': 422,
    'ValidationError': 422,
    'ConflictError': 409,
    'ExpirationError': 422,
    'CastError': 422
};

const errorProps = {
    'TokenExpiredError': { 'jwt': 'expired' }
};

module.exports = {
    errorStatusCodes: errorStatusCodes,
    errorProps: errorProps,

    genUNFE: (user) => {
        var error = new Error('UserNotFoundError');
        error.name = 'UserNotFoundError';
        error.status_code = 422;
        error.message = `user '${user}' not found`;
        error.err_props = {};
        return error;
    },

    genNFE: (name) => {
        var error = new Error('NotFoundError');
        error.name = 'NotFoundError';
        error.status_code = 422;
        error.message = `${name} not found`;
        error.err_props = {};
        return error;
    },

    genRE: (obj_name, r_props) => {
        var error = new Error('RequiredError');
        error.name = 'RequiredError';
        error.status_code = 422;
        error.message = `'${obj_name}'>'${r_props}' required`;
        error.r_props = r_props;
        return error;
    },

    // v1.6
    genVE: (obj_name, iv_props) => {
        var error = new Error('ValidationError');
        error.name = 'ValidationError';
        error.status_code = 422;
        error.message = `'${obj_name}'>'${iv_props}' invalid`;
        error.iv_props = iv_props;
        return error;
    },

    // v1
    // unqiue / conflict
    genCfE: (obj_name, obj_props, cfo_name, cfo_props) => {
        var cf_props = [];

        // get conflicting (shared) props, TODO: use lodash?
        for (var prop in obj_props) {
            //if(cfo_props.hasOwnProperty(prop) && obj_props.hasOwnProperty(prop)) {// (obj_props check cause js) checks if both have the same properties (doesn't even work) // EDIT: IDK?
            if (cfo_props[prop] == obj_props[prop]) {
                cf_props.push(prop);
            }
        }
        var error = new Error('ConflictError');
        error.name = 'ConflictError';
        error.status_code = 409;
        error.message = `'${obj_name}' conflicts with '${cfo_name}'`;
        // colors aren't understood by log4js (apparently)
        error.obj_props = util.inspect(obj_props, { depth: null, colors: false });
        error.cfo_props = util.inspect(cfo_props, { depth: null, colors: false });
        error.cf_props = cf_props;
        return error;
    },

    genEE: (obj_name, e_props) => {
        var error = new Error('ExpirationError');
        error.name = 'ExpirationError';
        error.status_code = 422;
        error.message = `'${obj_name}'>'${e_props}' expired`;
        error.e_props = e_props;
        return error;
    },

    genTRE: (obj_name) => {
        var error = module.exports.genRE(obj_name, 'token');
        //error.name = 'TokenRequiredError';
        error.status_code = 401;
        return error;
    },

    genTVE: (obj_name) => {
        var error = module.exports.genVE(obj_name, 'token');
        //error.name = 'TokenValidationError';
        error.status_code = 401;
        return error;
    },

    genTEE: (obj_name) => {
        var error = module.exports.genEE(obj_name, 'token');
        //error.name = 'TokenExpiredError';
        error.status_code = 401;
        return error;
    },

    // v1
    // status_codes aren't unique, 401 could be an expried token or a wrongly encoded one or even a missing one, the client will need a different response
    getPublicErrData: (err) => {
        //return { cf_props: err.cf_props };
        switch (err.name) {
            case 'RequiredError':
                if (!Array.isArray(err.r_props))
                    err.r_props = [err.r_props];
                var err_fields = {};
                for (var prop of err.r_props) {
                    err_fields[prop] = 'required';
                }
                return err_fields;
            case 'ValidationError':
                if (!Array.isArray(err.iv_props))
                    err.iv_props = [err.iv_props];
                var err_fields = {};
                for (var prop of err.iv_props) {
                    err_fields[prop] = 'invalid';
                }
                return err_fields;
            case 'ConflictError':
                if (!Array.isArray(err.cf_props))
                    err.cf_props = [err.cf_props];
                var err_fields = {};
                for (var prop of err.cf_props) {
                    err_fields[prop] = 'conflict';
                }
                return err_fields;
            case 'ExpirationError':
                if (!Array.isArray(err.e_props))
                    err.e_props = [err.e_props];
                var err_fields = {};
                for (var prop of err.e_props) {
                    err_fields[prop] = 'expired';
                }
                return err_fields;
            default:
                switch (err.message) {
                    case 'No auth token':
                        var err_fields = {
                            'jwt': 'required'
                        }
                        return err_fields;
                }
        }

        return errorProps[err.name] != undefined ? errorProps[err.name] : {};
    },

    // v1
    getErrStatusCode: (err) => {
        if (err.status_code)
            return err.status_code;;

        switch (err.message) {
            case 'No auth token':
                return 422;
        }

        return errorStatusCodes[err.name] != undefined ? errorStatusCodes[err.name] : 500;
    },

    errorHandler: (err, req, res, next) => {
        var task = 'errorHandler(err, req, res, next)';

        var publicErrData = module.exports.getPublicErrData(err);
        var statusCode = module.exports.getErrStatusCode(err);
        if (statusCode < 500)
            log.warn({ task: task, err: err, statusCode: statusCode, publicErrData: publicErrData });
        else
            log.error({ task: task, err: err, statusCode: statusCode, publicErrData: publicErrData });

        if (res.headersSent)
            return;

        res.status(statusCode).send({ error: err.name, isValid: false, iv_fields: publicErrData });
    },

    // v0.1-testing
    getDuplicates: (obj1, obj2) => { // untested
        var dups = [];
        for (var prop in obj1) {
            if (obj1[prop] == obj2[prop]) {
                dups.push(prop);
            }
        }
        return dups;
    },

    // V1
    getListOfObjects: (obj, list) => {
        var props = {};
        for (var prop of list) {
            //console.log('gloO', 'checking '+prop);
            if (obj.hasOwnProperty(prop)) {
                //console.log('gloO', 'obj has '+prop);
                props[prop] = obj[prop];
            }
        }
        return props;
    },

    isValidId: (id) => {
        return ObjectId.isValid(id);
    },

    //isPropIv: !var

    isValidObj: (vObj) => {
        return !(vObj.iv_props.length > 0);
    },

    //obj.hasOwnProperty(key) && !isPropIv(obj[key])
    // getValidObj v1.2
    // TODO: currenly, it can't be differentiated between required and invalid... -> reqrite this to be a middleware, generate RequiredError(ValidationError and call next(rr))
    // ToDo: think abou using _.pick
    // Note: don't use !, otherwise false fields oder fields with 0 will be wrong
    getValidObj: (obj, propHolder, primProps, secProps) => {
        //console.log('jv.gVO', { propHolder: propHolder, primProps: primProps });
        var ivPrimProps = [];
        for (var i = 0; i < primProps.length; i++) {
            var key = primProps[i],
                value = _.get(propHolder, key);
            //console.log(`[P${i}] key: ${key}, value: ${value}`);
            if (_.has(propHolder, key) && (typeof value !== 'undefined') && (typeof value !== null))
                _.set(obj, key, value); //'P'+
            else
                ivPrimProps.push(key);
        }
        for (var i = 0; i < secProps.length; i++) {
            var key = secProps[i],
                value = _.get(propHolder, key);
            //console.log(`[S${i}] key: ${key}, value: ${value}`);
            if (_.has(propHolder, key) && (typeof value !== 'undefined') && (typeof value !== null))
                _.set(obj, key, value); //'S'+
        }
        //console.dir(obj, { depth: null, colors: true });
        return {
            iv_props: ivPrimProps,
            obj: obj
        }
    },

    // test@mail.com email true
    // password minlength 8
    checkRequirement: (obj, rKey, rValue) => {
        var task = 'checkRequirement';
        switch (rKey) {
            // TODO: this is wrong! it doesn't allow 0, false, ...
            case 'required': // checks for null, undefined & ''
                return !!obj;
            case 'minlength':
                return obj.length >= rValue;
            case 'maxlength':
                return obj.length <= rValue;
            case 'email':
                return emailValidator.validate(obj) == rValue;
            case 'pattern':
            case 'regex':
                return (obj.match(rValue) && obj.match(rValue).length == 1);
            default:
                log.error({ task: task, mesg: 'unknown requirement \'' + rKey + '\'!' });
                return true;
        }
    },

    // objs: [ 'usernamey', 'pmypassword' ]
    // reqs: [ { minlength: 1, maxlength: 256 }, { minlength: 8, maxlength: 1024 } ]
    /*validate: (objs, reqs) => {
        var task = 'validate';

        var result = { isValid: true };

        if(objs.constructor !== Array) objs = [objs];
        if(reqs.constructor !== Array) reqs = [reqs];

        // in this implementation, there is no key vor the objs
        // objs: ['minusername', 'mypassword']
        // result: { minusername: 'minlength' }
        // not very helpful
        for(let o=0; o<objs.length; o++) {
            let j = o;
            if(j>=reqs.length) j = 0;

            console.log(task, 'o', objs[o], 'j', reqs[j]);
            for (var rKey in reqs[j]) {
                if (reqs[j].hasOwnProperty(rKey)) {
                    console.log(task, rKey + " -> " + reqs[j][rKey]);
                    let thisResut = module.exports.checkRequirement(objs[o], rKey, reqs[j][rKey]);
                    if(!thisResut) {
                        result.isValid = false;
                        // creaes an array with where each validity probelm takes up it's own object
                        result.iv_fields = result.iv_fields || [];
                        result.iv_fields.push({ [objs[o]]: rKey });
                    }
                }
            }
        }

        return result;
    },*/

    // objs: { username: 'test', password: 'mysecpass' }
    // reqs: { username: { minlength: 1, maxlength: 256 }, password: { minlength: 8, maxlength: 1024 } }
    validate: (objs, reqs) => {
        var task = 'validate';

        var result = { isValid: true };

        if (objs.constructor !== Array) objs = [objs];

        for (let o = 0; o < objs.length; o++) {
            for (var oKey in objs[o]) {
                //console.log(task, 'oKey', oKey);
                for (var rKey in reqs[oKey]) {
                    if (reqs[oKey].hasOwnProperty(rKey)) {
                        //console.log(task, rKey + " -> " + reqs[oKey][rKey]);
                        let thisResut = module.exports.checkRequirement(objs[o][oKey], rKey, reqs[oKey][rKey]);
                        if (!thisResut) {
                            result.isValid = false;
                            result.iv_fields = /* result.iv_fields ||*/ {};
                            result.iv_fields[oKey] = rKey;
                        }
                    }
                }
            }
        }

        return result;
    },

    /*
    getValidObj: (obj, propHolder, primProps, secProps) => {
    	var ivPrimProps = [];
    	for(var i=0; i<primProps.length; i++) {
    		var key = primProps[i], value = module.getPropertyByString(propHolder, key);
    		console.log(`[${i}] key: ${key}, value: ${value}`);
    		if(!module.isPropIv(value))
    			module.getPropertyByString(obj, key) = value;
    		else
    			ivPrimProps.push(key);
    	}

    	//Not yet ready!
    	for(var i=0; i<secProps.length; i++) {
    		var key=secProps[i], value = propHolder[key];
    		if(!module.isPropIv(value))
    			obj[key] = value;
    	}
    	return {
    		iv_props: ivPrimProps,
    		obj: obj
    	}
    },
    */

    /*
    //v1
    getValidObj: (obj, propHolder, primProps, secProps) => {
    	var ivPrimProps = [];
    	for(var i=0; i<primProps.length; i++) {
    		var key = primProps[i], value = propHolder[key];
    		if(!module.isPropIv(value))
    			obj[key] = value;
    		else
    			ivPrimProps.push(key);
    	}
    	for(var i=0; i<secProps.length; i++) {
    		var key=secProps[i], value = propHolder[key];
    		if(!module.isPropIv(value))
    			obj[key] = value;
    	}
    	return {
    		iv_props: ivPrimProps,
    		obj: obj
    	}
    },
    */

    /*
    http://stackoverflow.com/questions/6491463/accessing-nested-javascript-objects-with-string-key
    Just use lodash...?
    */
    getPropertyByString: function(o, s) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, ''); // strip a leading dot
        var a = s.split('.');
        for (var i = 0, n = a.length; i < n; ++i) {
            var k = a[i];
            if (k in o) {
                o = o[k];
            } else {
                return;
            }
        }
        return o;
    },

    isObject: (obj) => {
        return obj instanceof Object;
    },

    // proplem: validate is only called at the beginning, later it's only (req, res, next) (--> obj not submitted anymore)
    // IT DOESN'T WORK; DON'T TRY!
    /*
    validate: function(obj, reqParamName, primProps, secProps) {
    	console.log('arguments('+arguments.length+'): '+arguments);
    	console.log('obj: '+obj+', '+JSON.stringify(obj));
    	return function(req, res, next) {
    		console.log('arguments('+arguments.length+'): '+arguments);
    		console.log('obj: '+obj+', '+JSON.stringify(obj));
    		if(module.isPropIv(obj)) {
    			var error = new Error();
    			error.name = 'ValidationError';
    			error.status_code = 422;
    			error.message = 'obj not valid';
    			error.iv_props = ['obj'];
    			return next(error);
    		}
    		var vObj = module.getValidObj(obj, req[reqParamName], primProps, secProps);
    		//obj = {};//otherwise, fields that aren't overwritten keep their value (obj is now global...) PROBLEM: if obj was a userModel, it now isn't anymore!!
    		obj = undefined;
    		if(module.isValidObj(vObj)) {
    			req.vObj = vObj;
    			return next();
    		} else {
    			var error = new Error();
    			error.name = 'ValidationError';
    			error.status_code = 422;
    			error.message = 'vObj not valid';
    			error.iv_props = vObj.iv_props;
    			return next(error);
    		}
    	}
    }*/
}