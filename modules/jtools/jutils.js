var TAG = 'jutils';
var version = '0.1.7';

var mongoose = require('mongoose');
var crypto = require('crypto');

module.exports = {
    strToId: (str_id) => {
        return mongoose.Types.ObjectId(str_id);
    },

    /*logError: (logger, task, err) => {
        if (err.status_code)
            module.logErrorByStatuscode(logger, task, err);
        else
            module.logErrorByName(logger, task, err);
    },

    logErrorByStatuscode: (logger, task, err) => {
        if (err.status_code >= /*30*/
    /* 0 && err.status_code <= 399)
                logger.debug(task, { err: err }); // err?
            else if (err.status_code >= 400 && err.status_code <= 499)
                logger.warn(task, { err: err });
            else if (err.status_code >= 500 && err.status_code <= 599)
                logger.error(task, { err: err, stack: err.stack });
            else
                logger.error(task, { error: `unknown_err_status_code: ${err.status_code}`, err: err, stack: err.stack });
        },

        logErrorByName: (logger, task, err) => {
            logger.error(task, { error: 'no status_code provided' });
            switch (err.name) {
                case 'ValidationError':
                case 'ConflictError':
                    logger.warn(task, { err: err });
                    break;
                default:
                    logger.error(task, { err: err, stack: err.stack });
            }
        },

        getUFVEmsg: (field_name) => {
            return `Sorry, ${field_name} not valid`;
        },

        getUFCfEmsg: (field_name) => {
            return `Sorry, ${field_name} conflicting`;
        },*/

    getRandomNumber: (min, max) => {
        return Math.floor((Math.random() * max) + min);
    },

    /*getRandomStringAsBase64Url: (size) => {
        // TODO use own method
        return base64url(crypto.randomBytes(size));
    },*/

    unionRequired: (obj, props) => {

    },

    urlsafeBase64: (base64_string) => {
        return base64_string
            .replace(/\+/g, '-') // Convert '+' to '-'
            .replace(/\//g, '_') // Convert '/' to '_'
            .replace(/=+$/, ''); // Remove ending '='
    }
}