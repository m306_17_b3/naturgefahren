const TAG = 'jmodelfunctions';
const version = '1';

var _ = require('lodash');

var jvalidator = require('./jvalidator');

// ToDo: {email: 'xy', token: 'yx'} -> this email, in conjunction with this token, has to be unique
module.exports = {
    preSaveValidator: (propHolder, model, uniquePrimProps, uniqueSecProps, next) => {
        var task = 'jmf.pSV';

        if (!propHolder.isNew)
            return next();

        var availableUniqueProps = [],
            orCheck = [];

        var allUniqueProps = _.union(uniquePrimProps, uniqueSecProps);
        propHolder = propHolder.toObject();
        var iv_props = [];
        for (var uniqueProp of allUniqueProps) {
            if (propHolder.hasOwnProperty(uniqueProp)) {
                availableUniqueProps.push(uniqueProp);
                var tmpObj = {};
                tmpObj[uniqueProp] = propHolder[uniqueProp];
                orCheck.push(tmpObj);
                // check if the unqiue property is secondary (if yes, it doesn't have to exist)
            } else if (!(uniqueSecProps.indexOf(uniqueProp) > -1)) {
                iv_props.push(uniqueProp);
            }
        }
        // iv_props only contains primary _unique, not all primaries!
        if (iv_props.length > 0) {
            var error = jvalidator.genVE('propHolder', iv_props);
            return next(error);
        }
        // MongoError: $and/$or/$nor must be a nonempty array
        if (orCheck.length == 0) return next();
        // TODO: maybe, some property is conflicting with another doc and it gets returned, but there are still different models with different conflicts
        // > use find and add them to an array

        model.findOne({
            $or: orCheck
        }, function(err, doc) {
            if (err)
                return next(err);

            if (doc) {
                doc = doc.toObject();
                var propHolderUniquePropsDups = jvalidator.getListOfObjects(propHolder, allUniqueProps);
                var docUniquePropsDups = jvalidator.getListOfObjects(doc, allUniqueProps);
                var error = jvalidator.genCfE('model', propHolderUniquePropsDups, 'another doc', docUniquePropsDups);
                return next(error);
            }
            return next();
        });
    }
}