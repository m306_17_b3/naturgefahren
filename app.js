const TAG = 'app';
const NAME = 'concept_01';
const VERSION = '1';

var config = require('./config/config');

// suppper idee
//WichtigesStaatlichesProjekt
//WichtigerStaatlicherNutzer
//MnH517NC1hnLttgov
//var port = 443;
var port = process.env.PORT || config.port || 8082;

const bunyan = require('bunyan');
const bunyanMiddleware = require('bunyan-middleware');
const log = bunyan.createLogger({ name: `${TAG}V${VERSION}`, level: 'debug' });
const access_log = bunyan.createLogger({ name: 'access', level: 'debug' });

log.debug({ task: 'init', mesg: `${TAG}@${NAME}_v${VERSION} on ${process.platform} ${process.arch} running Node.JS ${process.version} env ${process.env.NODE_ENV} with pid ${process.pid} in ${process.cwd()}` });

const express = require('express');
const app = express();
const http = require('http').Server(app);

const _ = require('lodash');
const ejs = require('ejs');
const helmet = require('helmet');
const morgan = require('morgan');
const mongoose = require('mongoose');
const passport = require('passport');
const bodyParser = require('body-parser');
const session = require('express-session');
const webTerminal = require("web-terminal");
const cookieParser = require('cookie-parser');
const forceSsl = require('express-force-ssl');
const MongoStore = require('connect-mongo')(session);

/*
MAYBE:
Mongoose.connect(config.db);

const db = Mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function callback() {
    console.log("Connection with database succeeded.");
});
*/
mongoose.Promise = global.Promise;
const mongo_connection = mongoose.connect(config.database, {
    useMongoClient: true,
    promiseLibrary: global.Promise
}).then(() => {
    log.debug({ task: 'mongoose.connect' });
}).catch(err => {
    log.error({ task: 'mongoose.connect', err: err });
});

const jModelFunctions = require('./modules/jtools/jmodelfunctions');

require('./app/models/user')(mongoose, jModelFunctions.preSaveValidator);
require('./app/models/map')(mongoose, jModelFunctions.preSaveValidator);
require('./app/auth/passport')();

app.use(morgan('dev'));
//app.use(bunyanMiddleware({ logger: access_log }));
app.use(cookieParser(config.secrets.cookies));
// limit: '52428800'
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(session({
    key: 'user_sid',
    secret: config.secrets.cookies,
    resave: false,
    saveUninitialized: true,
    maxAge: 365 * 24 * 60 * 60 * 1000,
    //cookie: { secure: true }
    //cookie: { expires: 1000 * 60 * 120 }
    store: new MongoStore({ mongooseConnection: mongoose.connection })
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(helmet());
// dev-edit
//app.use(forceSsl);

app.set('view engine', 'html');
app.set('views', [__dirname + '/public', __dirname + '/private']);
app.engine('html', ejs.renderFile);

app.use(express.static(__dirname + '/public'));

require('./app/router.js')(app);
http.listen(port, () => {
    log.debug({ task: 'http.listen', port: port, uptime: process.uptime() });
});

// debugging
//webTerminal(http);

// stacktrace not shown
/*process.on('uncaughtException', (err) => {
    log.error({ task: 'process.onUncaughtException', err: err });
    // TODO either use clusters or notify the admin somehow
    // NOTE don't keep running, we are now in an undefined state
    process.exit(1);
});*/

module.exports = app;