const TAG = 'webV1';
const VERSION = '1';

const config = require('../../../config/config');

const jValidator = require('../../../modules/jtools/jvalidator');

module.exports = (passport) => {
    const router = require('express').Router();

    const api = require(`./v${VERSION}/api`)(passport);

    router.use('/api', api);

    router.get('/', (req, res) => {
        return res.send({ version: VERSION });
    });

    return router;
}