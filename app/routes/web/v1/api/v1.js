const TAG = 'webV1_apiV1';
const VERSION = '1';

const config = require('../../../../../config/config');

const jValidator = require('../../../../../modules/jtools/jvalidator');

module.exports = (passport) => {
    const router = require('express').Router();

    const auth = require(`./v${VERSION}/auth`)(passport);
    const charts = require(`./v${VERSION}/charts`)(passport);
    const share = require(`./v${VERSION}/share`)(passport);
    const maps = require(`./v${VERSION}/maps`)(passport);

    router.use('/auth', auth);
    router.use('/charts', charts);
    router.use('/share', share);
    router.use('/maps', maps);

    router.get('/', (req, res) => {
        return res.send({ version: VERSION });
    });

    return router;
}