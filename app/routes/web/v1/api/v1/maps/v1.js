const TAG = 'webV1_apiV1_shareV1';
const VERSION = '1';

const config = require('../../../../../../../config/config');

const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: `${TAG}V${VERSION}`, level: 'debug' });

const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mongoose = require('mongoose');
const shapefile = require('shapefile');
const formidable = require('formidable');

const jUtils = require('../../../../../../../modules/jtools/jutils');
const jValidator = require('../../../../../../../modules/jtools/jvalidator');

const authWeb = require('../../../../../../auth/auth-web');
const coordsTools = require('../../../../../../tools/coords');
const mapsTools = require('../../../../../../tools/maps');

const mapModel = mongoose.model('map');

module.exports = (passport) => {
    const router = require('express').Router();

    /*
    NOTE:
    - with the data we get it seems to be the easiest to convert all of it (shp + dbf) to geojson and use that in OpenLayers
    */

    router.get('/', (req, res, next) => {
        let task = 'GET /';

        mapModel.find({}, config.models.map.projections.web, (err, mapDocs) => {
            if (err) return next(err);
            if (!mapDocs) return next(jValidator.genNFE('maps'));

            res.send(mapDocs);
        });
    });

    // TODO multiple uploads (.shp, .dbf, .shx, ...)
    // NOTE: fields such as display_name only available after file has been uploaded
    // the db entry can only be saved when all data is available
    router.post('/geojson', authWeb.ensureAuthenticatedAPI, (req, res, next) => {
        let task = 'POST /geojson';

        var uploadDir = path.join(__dirname, '../../../../../../../public/uploads/geojson');
        log.debug({ task: task, uploadDir: uploadDir });

        var submittedMap = {};
        submittedMap.created_by = req.user._id;

        var form = new formidable.IncomingForm();
        form.uploadDir = uploadDir;
        form.on('file', (field, file) => {
            log.debug({ task: task + ' - file' });
            var mapVObj = jValidator.getValidObj(new mapModel({}), submittedMap,
                config.models.map.properties.primary, config.models.map.properties.secondary);
            if (jValidator.isValidObj(mapVObj)) {
                mapVObj.obj.save((err, saveMapDoc) => {
                    if (err) return done(err);
                    if (!saveMapDoc) return done(jValidator.genVE('mapVObj.obj.save', 'saveMapDoc'));

                    var saveMapObj = saveMapDoc.toObject();
                    fs.rename(file.path, path.join(form.uploadDir, saveMapObj._id + '.geojson'));

                    res.status(201).send(saveMapObj);
                });
            } else next(jValidator.genVE('submittedMap', mapVObj.iv_props));
        });
        form.on('field', function(field, value) {
            log.debug({ task: task + ' - field', field, value: value });
            submittedMap[field] = value;
        });
        form.on('fileBegin', function(name, file) {
            log.debug({ task: task + ' - fileBegin', file_name: file.name });
        });
        form.on('progress', function(bytesReceived, bytesExpected) {
            var percent = (bytesReceived / bytesExpected * 100) | 0;
            log.debug({ task: task, mesg: 'Uploading: ' + percent + '% (' + bytesReceived + '/' + bytesExpected + ')' });
        });
        form.on('error', (err) => {
            log.error({ task: task, err: err });
        });
        form.on('aborted', (err) => {
            log.warn({ task: task, mesg: 'upload aborted' });
        });
        form.on('end', () => {
            log.debug({ task: task + ' - end' });
            //res.send(_.pick(saveMapObj, config.models.map.projections.web));
        });
        form.parse(req);

        /*var form = new formidable.IncomingForm();
        form.keepExtensions = true;
        form.parse(req, (err, fields, files) => {
            if (!files.map) return next(jValidator.genRE('body', 'map'));
            console.log('files.map.path', files.map.path);
            mapsTools.getMapData(files.map.path, (mapErr, mapData) => {
                if (mapErr) return next(mapErr);

                var submittedMap = _.pick(fields, config.models.map.properties.submittable);
                submittedMap.created_by = req.user._id;

                // TODO: parse to wgs84
                console.log(`mapData: ${util.inspect(mapData)}`);

                var mapVObj = jValidator.getValidObj(new mapModel({}), submittedMap,
                    config.models.map.properties.primary, config.models.map.properties.secondary);
                if (jValidator.isValidObj(mapVObj)) {
                    mapVObj.obj.save((err, saveDoc) => {
                        if (err) return next(err);
                        if (!saveDoc) return next(jValidator.genVE('mapVObj.obj.save', 'saveDoc'));

                        res.send({ success: true });
                    });
                } else {
                    var error = jValidator.genVE('submittedMap', mapVObj.iv_props);
                    next(error);
                }
            });
        });*/
    });

    router.get('/:mapId', (req, res, next) => {
        mapModel.findById(jUtils.strToId(req.params.mapId), config.models.map.projections.web, (err, mapDoc) => {
            if (err) return next(err);
            if (!mapDoc) return next(jValidator.genNFE('map-' + req.params.mapId));

            res.send(mapDoc);
        });
        /*shapefile.open("./res/Gefahrenhinweiskarte/ueberflutung.shp")
            .then(source => source.read()
                .then(function log(result) {
                    if (result.done) return;

                    /*var map = {
                        _id: 'blub',
                        type: '0',
                        created_by: '@+id/123',
                        display_name: 'TestMap',
                        map_date: Date.now(),
                        map_office: result.value.properties,
                        map_state: result.value.properties,
                        map_basis: result.value.properties,
                        map_geometry: result.value.geometry
                    };

                    res.send(map);*/

        //console.log(result.value.geometry /*.coordinates[0]*/ ); // .geometry.coordinates[0]
        //return source.read().then(log);


        //   }))
        //.catch(error => next(jValidator.genVE('req', 'req')));
    });

    // how the fuck are we supposed to read the data automatically if the amount of nested arrays is different every-fucking-where

    return router;
}