const TAG = 'webV1_apiV1_chartsV1';
const VERSION = '1';

const config = require('../../../../../../../config/config');

const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: `${TAG}V${VERSION}`, level: 'debug' });

const jValidator = require('../../../../../../../modules/jtools/jvalidator');

const bgColors = {
    red: 'rgba(255, 99, 132, 0.2)',
    blue: 'rgba(54, 162, 235, 0.2)',
    yellow: 'rgba(255, 206, 86, 0.2)',
    green: 'rgba(75, 192, 192, 0.2)',
    purple: 'rgba(153, 102, 255, 0.2)',
    orange: 'rgba(255, 159, 64, 0.2)'
};

const borderColors = {
    red: 'rgba(255, 99, 132, 1)',
    blue: 'rgba(54, 162, 235, 1)',
    yellow: 'rgba(255, 206, 86, 1)',
    green: 'rgba(75, 192, 192, 1)',
    purple: 'rgba(153, 102, 255, 1)',
    orange: 'rgba(255, 159, 64, 1)'
};

module.exports = (passport) => {
    const router = require('express').Router();

    router.use('/test', (req, res) => {
        // xSteps (e.g. 10)
        // ySteps
        // ...
        // xStart
        // xEnd
        res.send([{
                name: "BarTest",
                desc: "A bar test",
                type: 'bar',
                data: {
                    labels: ["Red", "Blue", "Yellow", "Green", "Purple"],
                    datasets: [{
                        label: 'Simple Bars',
                        data: [4, 5, 6, 7, 8],
                        backgroundColor: [
                            bgColors.red,
                            bgColors.blue,
                            bgColors.yellow,
                            bgColors.green,
                            bgColors.purple
                        ],
                        borderColor: [
                            borderColors.red,
                            borderColors.blue,
                            borderColors.yellow,
                            borderColors.green,
                            borderColors.purple
                        ]
                    }, {
                        label: 'Advanced Bars',
                        data: [12, 4, 9, 26, 11],
                        backgroundColor: [
                            bgColors.orange,
                            bgColors.orange,
                            bgColors.orange,
                            bgColors.orange,
                            bgColors.orange
                        ],
                        borderColor: [
                            borderColors.orange,
                            borderColors.orange,
                            borderColors.orange,
                            borderColors.orange,
                            borderColors.orange
                        ]
                    }],
                    borderWidth: 1
                },
                options: {}
            }
            /*, {
                        name: 'LineTest',
                        desc: 'A line test',
                        type: 'line',
                        data: {
                            datasets: [

                            ]
                        },
                        options: {}
                    }*/
        ]);
    });

    router.get('/', (req, res) => {
        return res.send({ version: VERSION });
    });

    return router;
}