const TAG = 'webV1_apiV1_charts';
const VERSION = '1';

const config = require('../../../../../../config/config');

const jValidator = require('../../../../../../modules/jtools/jvalidator');

module.exports = (passport) => {
    const router = require('express').Router();

    const chartsV1 = require(`./charts/v${VERSION}`)(passport);

    router.use('/v1', chartsV1);

    router.get('/', (req, res) => {
        return res.send({ version: VERSION });
    });

    return router;
}