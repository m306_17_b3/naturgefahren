const TAG = 'webV1_apiV1_shareV1';
const VERSION = '1';

const config = require('../../../../../../../config/config');

const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: `${TAG}V${VERSION}`, level: 'debug' });

const jValidator = require('../../../../../../../modules/jtools/jvalidator');

const fs = require('fs-extra');
const path = require('path');
const formidable = require('formidable');

module.exports = (passport) => {
    const router = require('express').Router();

    router.post('/u', (req, res) => {
        var task = 'POST /u';
        var form = new formidable.IncomingForm();
        form.uploadDir = path.join(__dirname, '../../../uploads');
        form.on('file', function(field, file) {
            fs.rename(file.path, path.join(form.uploadDir, file.name));
        });
        form.on('error', function(err) {
            log.error({ task: task, err: err });
        });
        form.on('end', function() {
            res.end('success');
        });
        form.parse(req);
    });

    router.get('/u/:file_name', (req, res) => {
        res.sendFile(path.join(__dirname, '../../../uploads/' + req.params.file_name));
    });

    router.get('/', (req, res) => {
        return res.send({ version: VERSION });
    });

    return router;
}