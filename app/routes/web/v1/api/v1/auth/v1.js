const TAG = 'webV1_apiV1_authV1';
const VERSION = '1';

const config = require('../../../../../../../config/config');

const _ = require('lodash');

const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: `${TAG}V${VERSION}`, level: 'debug' });

const authWeb = require('../../../../../../auth/auth-web');

const jValidator = require('../../../../../../../modules/jtools/jvalidator');

module.exports = (passport) => {
    const router = require('express').Router();

    router.post('/login', (req, res, next) => {
        passport.authenticate(`local-login`, (err, user, info) => {
            if (err) return next(err);
            if (info instanceof Error) return next(info);

            req.login(_.pick(user, config.models.user.projections.serialize), (err) => {
                if (err) return next(err);
                next();
            });
            next();
        })(req, res, next);
    }, (req, res) => {
        res.send(JSON.stringify(req.user));
    } /*, jValidator.errorHandler*/ );

    router.post('/register', (req, res, next) => {
        passport.authenticate(`local-register`, (err, user, info) => {
            if (err) return next(err);
            if (info instanceof Error) return next(info);
            if (!user) return next(jValidator.genVE('user', []));

            req.login(_.pick(user, config.models.user.projections.serialize), (err) => {
                if (err) return next(err);
                next();
            });
        })(req, res, next);
    }, (req, res) => {
        res.send(JSON.stringify(req.user));
    } /*, jValidator.errorHandler*/ );

    router.get('/me', authWeb.ensureAuthenticatedAPI, (req, res) => {
        res.send(JSON.stringify(req.user));
    });

    router.get('/', (req, res) => {
        return res.send({ version: VERSION });
    });

    return router;
}