const TAG = 'webV1_apiV1_maps';
const VERSION = '1';

const config = require('../../../../../../config/config');

const jValidator = require('../../../../../../modules/jtools/jvalidator');

module.exports = (passport) => {
    const router = require('express').Router();

    const shareV1 = require(`./maps/v${VERSION}`)(passport);

    router.use('/v1', shareV1);

    router.get('/', (req, res) => {
        return res.send({ version: VERSION });
    });

    return router;
}