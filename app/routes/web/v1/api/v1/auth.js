const TAG = 'webV1_apiV1_auth';
const VERSION = '1';

const config = require('../../../../../../config/config');

const jValidator = require('../../../../../../modules/jtools/jvalidator');

module.exports = (passport) => {
    const router = require('express').Router();

    const authV1 = require(`./auth/v${VERSION}`)(passport);

    router.use('/v1', authV1);

    router.get('/', (req, res) => {
        return res.send({ version: VERSION });
    });

    return router;
}