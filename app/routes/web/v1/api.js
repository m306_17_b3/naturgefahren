const TAG = 'webV1_api';
const VERSION = '1';

const config = require('../../../../config/config');

const jValidator = require('../../../../modules/jtools/jvalidator');

module.exports = (passport) => {
    const router = require('express').Router();

    const apiV1 = require('./api/v1')(passport);

    router.use((req, res, next) => {
        res.header('Content-Type', 'application/json');
        next();
    });

    router.use('/v1', apiV1);

    router.get('/', (req, res) => {
        return res.send({ version: VERSION });
    });

    router.use((req, res, next) => {
        return res.status(404).send({ error: 'not found' });
    });

    router.use(jValidator.errorHandler);

    router.use((err, req, res, next) => {
        let task = '(err, req, res, next)';

        log.error({ task: task, mesg: 'EMERGENCY FALLBACK', err: err });
        return res.status(500).send({ error: 'unknown' });
    });

    return router;
}