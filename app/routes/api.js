const TAG = 'api';
const VERSION = '1';

const config = require('../../config/config');

const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: `${TAG}V${VERSION}`, level: 'debug' });

const jValidator = require('../../modules/jtools/jvalidator');

module.exports = (passport) => {
    const router = require('express').Router();

    // Point (coordinates: x, y)
    // Polygon [[coordinates: x, y]]
    // Line [[coordinates: x, y]] (connect all point with each other)
    // -> DatasetRow, which has an array of [x, y] coordinates

    /*
    Point:
    - steinschlag [properties: { objectid: 23, nr: 0 }]
    Polygon:
    - hochwasser [properties:
   { objectid: 383,
     handle: null,
     layer: null,
     sourcethm: 'hwlmugp.shp',
     area_: 0,
     perimeter: 0,
     hectares: 0,
     typ: null,
     name: 'Aach',
     datum: 20021014,
     buero: 'FrÃ¶hlich Wasserbau',
     stand: 'Vernehmlassung 2005',
     grundlage: '5\'000er' }]
    - rutschungen: [properties:
    { objectid: 12109,
     aktivitaet: 'sw',
     gruendigke: 'm',
     flaechen: null,
     sourcethm: 'Basis_Modell_406',
     area_: 0,
     perimeter: 0,
     hectares: 0,
     typ: 'Rutschung',
     stand: 'Vernehmlassung 2005',
     grundlage: '5\'000er' }]
    - ueberflutung [properties:
    { objectid: 1121,
     aktivitaet: null,
     gruendigke: null,
     flaechen: null,
     sourcethm: 'L17_BachrÃ¼ckstau',
     area_: 77934.902,
     perimeter: 1513.241,
     hectares: 7.793,
     grundlage: '5\'000er',
     stand: 'Vernehmlassung 2005',
     typ: 'Ãœberflutung' }]
    -
    Line:

    Since the avail. properties pretty inconsistent, we just store them as object in MongoDB.
    The frontend will only show the available ones...
    */

    //const apiV1 = require('./api/v1')(passport);

    router.use((req, res, next) => {
        res.header('Cache-Control', 'no-cache');
        res.header('Content-Type', 'application/json');
        next();
    });

    //router.use('/v1', apiV1);

    router.get('/', (req, res) => {
        return res.send({ version: VERSION });
    });

    router.use((req, res, next) => {
        return res.status(404).send({ error: 'not found' });
    });

    router.use(jValidator.errorHandler);

    router.use((err, req, res, next) => {
        let task = '(err, req, res, next)';
        log.error({ task: task, err: err });
        return res.status(500).send({ error: 'unknown' });
    });

    return router;
}