const TAG = 'web';
const VERSION = '1';

const config = require('../../config/config');

const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: `${TAG}V${VERSION}`, level: 'debug' });

const jValidator = require('../../modules/jtools/jvalidator');

module.exports = (passport) => {
    const router = require('express').Router();

    const webV1 = require('./web/v1')(passport);

    router.use((req, res, next) => {
        res.header('Cache-Control', 'no-cache');
        next();
    });

    router.use('/v1', webV1);

    router.get('/', (req, res) => {
        return res.send({ version: VERSION });
    });

    router.use((req, res, next) => {
        return res.status(404).send({ error: 'not found' });
    });

    router.use(jValidator.errorHandler);

    router.use((err, req, res, next) => {
        let task = '(err, req, res, next)';
        log.error({ task: task, err: err });
        return res.status(500).send({ error: 'unknown' });
    });

    return router;
}