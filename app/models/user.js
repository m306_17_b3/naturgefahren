const TAG = 'user';
const VERSION = '1';

const config = require('../../config/config');

const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: `${TAG}V${VERSION}`, level: 'debug' });

const bcrypt = require('bcrypt');

module.exports = function(mongoose, preSaveValidator) {
    const module = {};

    // history of every change
    module.userSchema = new mongoose.Schema({
        type: { $mongo_type: Number, default: 1 }, // 0 - nothing, 1 - user (default c(req. verif.)/r/u(req. verif. exc. own)), 2 - admin (c/r/u/d(req. verif. exc. own) maps, 3 - root (everything))
        created_at: { $mongo_type: Date, default: Date.now },
        email: { $mongo_type: String, lowercase: true, required: false },
        pass: { $mongo_type: String, required: true },
    }, { collection: `${TAG}s`, typeKey: '$mongo_type' });

    module.userSchema.methods.comparePass = function(candidatePass, cb) {
        log.info({ task: 'user.comparePass', mesg: 'cP: ' + candidatePass });
        bcrypt.compare(candidatePass, this.pass, function(err, isMatch) {
            if (err) return cb(err);
            cb(null, isMatch);
        });
    };

    module.userSchema.pre('save', function(next) {
        return preSaveValidator(this, module.model,
            config.models.user.properties._unique.primary,
            config.models.user.properties._unique.secondary, next);
    });

    module.userSchema.pre('save', function(next) {
        var self = this;

        if (!self.isModified('pass')) return next();

        bcrypt.genSalt(15, function(err, salt) {
            if (err) return next(err);
            bcrypt.hash(self.pass, salt, function(err, hash) {
                if (err) return next(err);
                self.pass = hash;
                next();
            });
        });
    });

    module.model = mongoose.model(TAG, module.userSchema);

    return module.model;
};