const TAG = 'BasicChart';
const VERSION = '1';

const config = require('../../config/config');

const log4js = require('log4js');
const logger = log4js.getLogger(`${TAG}_v${VERSION}`);
logger.setLevel('ALL');

module.exports = (schema, model, preSaveValidator) => {
    var module = {};

    module.yeeSchema = new schema({
        type: { $mongo_type: String, required: true },
        created_at: { $mongo_type: Date, default: Date.now },
        created_by: { $mongo_type: String, required: true },
        name: { $mongo_type: String, required: true },
        data: { $mongo_type: Object, required: true },
        options: { $mongo_type: Object, required: true },
    }, { collection: `${TAG}s`, typeKey: '$mongo_type' });

    module.model = model(TAG, module.yeeSchema);
    //module.preSaveValidator = preSaveValidator;

    /*module.yeeSchema.pre('save', function(next) {
    	return module.preSaveValidator(this, module.model,
    		config.models.yee.properties._unique.primary,
    		config.models.yee.properties._unique.secondary)(next);
    });*/

    //return module;
    return module.model;
};