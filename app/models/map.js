const TAG = 'map';
const VERSION = '0.1.0';

const config = require('../../config/config');

const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: `${TAG}V${VERSION}`, level: 'debug' });

module.exports = (mongoose, preSaveValidator) => {
    const module = {};

    // TODO frontend should be capable of letting users add arbitrary fields stored in 'fields'
    // -> e.g. fields: { cooles_wichtiges_feld: '123443' }
    module.mapSchema = new mongoose.Schema({
        type: { $mongo_type: Number, default: 0 },
        created_at: { $mongo_type: Date, default: Date.now },
        created_by: { $mongo_type: String, required: true },
        display_name: { $mongo_type: String, required: true },
        description: { $mongo_type: String, required: true },
        map_date: { $mongo_type: Date, required: false },
        map_office: { $mongo_type: String, required: false }, // 'FrÃ¶hlich Wasserbau'
        map_state: { $mongo_type: String, required: false }, // 'Vernehmlassung 2005'
        map_basis: { $mongo_type: String, required: false }, // '5\'000er'
    }, { collection: `${TAG}s`, typeKey: '$mongo_type' });

    module.model = mongoose.model(TAG, module.mapSchema);
    module.preSaveValidator = preSaveValidator;

    module.mapSchema.pre('save', function(next) {
        return module.preSaveValidator(this, module.model,
            config.models.map.properties._unique.primary,
            config.models.map.properties._unique.secondary, next);
    });

    return module.model;
};