const TAG = 'auth-web';
const VERSION = '1';

const config = require('../../config/config');

const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: `${TAG}V${VERSION}`, level: 'debug' });

module.exports = {
    ensureAuthenticated: (req, res, next) => {
        var task = 'ensureAuthenticated';
        var authenticated = req.isAuthenticated() && !(req.user == null);
        log.debug({ task: task, authenticated: authenticated });
        if (authenticated) return next();
        res.redirect('/auth');
    },
    ensureAuthenticatedAPI: (req, res, next) => {
        var task = 'ensureAuthenticatedAPI';
        var authenticated = req.isAuthenticated() && !(req.user == null);
        log.debug({ task: task, authenticated: authenticated });
        if (authenticated) return next();
        res.status(401).send({ error: 'unauthenticated' });
    },
    ensureUnauthenticated: (req, res, next) => {
        var task = 'ensureUnauthenticated';
        var authenticated = req.isAuthenticated() && !(req.user == null);
        log.debug({ task: task, authenticated: authenticated });
        if (!authenticated) return next();
        res.redirect('/create-map');
    },
    ensureUnauthenticatedAPI: (req, res, next) => {
        var task = 'ensureUnauthenticatedAPI';
        var authenticated = req.isAuthenticated() && !(req.user == null);
        log.debug({ task: task, authenticated: authenticated });
        if (!authenticated) return next();
        res.status(401).send({ error: 'authenticated', });
    },
};