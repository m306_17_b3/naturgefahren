const TAG = 'passport';
const VERSION = '1';

const config = require('../../config/config');

const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: `${TAG}V${VERSION}`, level: 'debug' });

const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const jValidator = require('../../modules/jtools/jvalidator');

const userModel = mongoose.model('user');

// MAYBE: if passport can be importet everywhere without having to pass it around
module.exports = ( /*passport*/ ) => {
    passport.serializeUser((user, done) => {
        // done(null, user._id);
        done(null, user);
    });

    passport.deserializeUser((user, done) => {
        /*userModel.findById(id, (err, user) => {
            done(err, user);
        });*/
        done(null, user);
    });

    passport.use('local-register', new LocalStrategy({ usernameField: 'email', passwordField: 'pass', passReqToCallback: true }, (req, email, pass, done) => {
        let task = `local-register(req, email: ${email}, pass, done)`;
        log.debug({ task: task });

        process.nextTick(() => {
            var submittedUser = { email: email, pass: pass };

            var userVObj = jValidator.getValidObj(new userModel({}), submittedUser,
                config.models.user.properties.primary, config.models.user.properties.secondary);
            if (jValidator.isValidObj(userVObj)) {
                userVObj.obj.save((err, saveUserDoc) => {
                    if (err) return done(err);
                    if (!saveUserDoc) return done(jValidator.genVE('userVObj.obj.save', 'saveUserDoc'));

                    var saveUserObj = saveUserDoc.toObject();
                    return done(null, saveUserObj);
                });
            } else done(jValidator.genVE('submittedUser', userVObj.iv_props));
        });
    }));

    passport.use('local-login', new LocalStrategy({ usernameField: 'email', passwordField: 'pass', passReqToCallback: true }, (req, email, pass, done) => {
        let task = `local-login(req, email: ${email}, pass, done)`;
        log.debug({ task: task });

        process.nextTick(() => {
            userModel.findOne({ email: email }, (err, userDoc) => {
                if (err) return done(err);
                if (!userDoc) return done(jValidator.genUNFE(email));

                userDoc.comparePass(pass, (err, isMatch) => {
                    if (err) return done(err);
                    if (!isMatch) return done(jValidator.genUNFE(email));

                    var userObj = userDoc.toObject();
                    return done(null, userObj);
                });
            });
        });
    }));
};
//module.exports = passport;a