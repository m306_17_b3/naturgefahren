const TAG = 'tools_maps';
const VERSION = '1';

const config = require('../../config/config');

const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: `${TAG}V${VERSION}`, level: 'debug' });

const shapefile = require("shapefile");

module.exports = {
    getMapData: (mapPath, cb) => {
        shapefile.open(mapPath)
            .then(source => source.read()
                .then(function log(result) {
                    if (result.done) return;
                    cb(null, { map_geometry: result.value.geometry });
                }))
            .catch(err => cb(err));
    }
};