const TAG = 'tools_coords';
const VERSION = '1';

const config = require('../../config/config');

const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: `${TAG}V${VERSION}`, level: 'debug' });

// lng (x) / lat (y)
module.exports = {
    // https://www.damianbrunold.ch/2011/4/28_Umrechnung_Koordinaten_WGS84-CH1903.html
    // das staatl. dok
    CHtoWGS84lat: (y, x) => {
        var y_aux = (y - 2600000) / 1000000;
        var x_aux = (x - 1200000) / 1000000;

        lat = 16.9023892 +
            3.238272 * x_aux -
            0.270978 * Math.pow(y_aux, 2) -
            0.002528 * Math.pow(x_aux, 2) -
            0.0447 * Math.pow(y_aux, 2) * x_aux -
            0.0140 * Math.pow(x_aux, 3);

        return lat * 100 / 36;
    },

    CHtoWGS84lng: (y, x) => {
        var y_aux = (y - 2600000) / 1000000;
        var x_aux = (x - 1200000) / 1000000;

        lng = 2.6779094 +
            4.728982 * y_aux +
            0.791484 * y_aux * x_aux +
            0.1306 * y_aux * Math.pow(x_aux, 2) -
            0.0436 * Math.pow(y_aux, 3);

        return lng * 100 / 36;
    },

    // https://gist.github.com/springmeyer/871897
    /*
    // convert from long/lat to google mercator (or EPSG:4326 to EPSG:900913)
    var degrees2meters = function(lon,lat) {
        var x = lon * 20037508.34 / 180;
        var y = Math.log(Math.tan((90 + lat) * Math.PI / 360)) / (Math.PI / 180);
        y = y * 20037508.34 / 180;
        return [x, y]
    }

    x= -77.035974
    y = 38.898717

    console.log(degrees2meters(x,y))

    // should result in: -8575605.398444, 4707174.018280
    */
    // https://epsg.io/transform#s_srs=4326&t_srs=3857&x=8.9852489&y=47.6353892 (code on github, maybe we could check it out)
    WGS84toEPSG: (lng, lat) => {
        var x = lng * 20037508.34 / 180;
        var y = Math.log(Math.tan((90 + lat) * Math.PI / 360)) / (Math.PI / 180);
        y = y * 20037508.34 / 180;
        return [x, y];
    }
};