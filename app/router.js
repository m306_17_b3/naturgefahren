const TAG = 'router';
const VERSION = '1';

const config = require('../config/config');
const passport = require('passport');

const bunyan = require('bunyan');
const log = bunyan.createLogger({ name: `${TAG}V${VERSION}`, level: 'debug' });

const jValidator = require('../modules/jtools/jvalidator');
const authWeb = require('./auth/auth-web');

module.exports = (app) => {

    /*
    TODO:
    - sort maps by tags? (#gefahr #verkehr)
    - map.html: map overlay
    - auth.html: design (form (email, pass), little info, etc.)
    - private/upload.html: making a usable request (can file & other form fields be combined)

    TODO-SERVER:
    - maps/v1: parse map, handle data

    MANAGEMENT:
    - http upload the shp file (/admin/add-map) (file, name of map, tags)

    IDEA:
    - Routenplaner
    - BauPlaner: Radius von 100m, besten Baugrund finden
    - App/website die einen via location warnt
    - alles kann bearbeitet werden (jeder kann änderungen vorschlagen/sachen hinzufügen (veified users müssen nicht auf approval warten))

    NOTE:
    - since the static server comes before the router (router has a catch-all), every path which requires auth can still be accessed by adding a .html to the url
    - that's why we have a privae folder, which can only be accessed via render()
    - passport: no redirects - json, session - yes

    DEFAULT-NOTE:
    - public & private directory: anyone can open the .html file even without auth (static serving is not checked). even though api requests will still fail it's ugly
    - Ajax instead of form request: less traffic -> increased perf
    - custom handler -> json response (no 'Bad Request)

    LINKS:
    - https://openlayers.org/en/latest/examples/overlay.html
    - IDEA: directly load shapefile into openlayers - WEEKEND!
    - https://gis.stackexchange.com/questions/166594/uploading-shp-kml-csv-gml-using-openlayers
    - https://gis.stackexchange.com/questions/132452/why-my-shapefile-is-not-loaded-into-openlayers
    - http://www.cv.nctu.edu.tw/tonywang/shapefile-js-openlayers/ol_simple.html
    - https://stackoverflow.com/questions/10692075/which-library-should-i-use-for-server-side-image-manipulation-on-node-js

    BASIC-LINKS:
    - https://openlayersbook.github.io/ch02-key-concepts-in-openlayers/example-01.html
    - https://openlayers.org/en/latest/examples/center.html

    -----

    { map_geometry:
   { type: 'LineString',
     coordinates:
      [ [Array],
        [Array],
        [Array],
        [Array],
        [Array],
        [Array],
        [Array],
        [Array],
        [Array],
        [Array],
        [Array],
        [Array] ] } }
    
    mapData: { map_geometry:
   { type: 'Point',
     coordinates: [ 2747344.4171250015, 1262754.2451250032 ] } }

     mapData: { map_geometry: { type: 'Polygon', coordinates: [ [Array], [Array] ] } }
    */

    const api = require('./routes/api')(passport);
    const web = require('./routes/web')(passport);

    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', req.headers.origin);

        res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, UPDATE, PATCH, DELETE');
        res.header('Access-Control-Allow-Headers', 'Origin, Referer, User-Agent, X-Requested-With, Content-Type, Accept, Authorization, jwt, refreshToken');
        res.header('Access-Control-Allow-Credentials', true);

        next();
    });

    app.get('/', (req, res) => {
        res.render('index');
    });

    app.use('/api', api);
    app.use('/web', web);

    app.use('/auth', authWeb.ensureUnauthenticated, (req, res) => {
        res.render('auth');
    });

    app.use('/map', (req, res) => {
        res.render('map');
    });

    app.use('/faq', (req, res) => {
        res.render('faq');
    });

    app.use('/aboutUs', (req, res) => {
        res.render('aboutUs');
    });

    app.use('/dashboard', authWeb.ensureAuthenticated, (req, res) => {
        res.render('dashboard');
    });

    app.use('/create-map', authWeb.ensureAuthenticated, (req, res) => {
        res.render('create-map');
    });

    app.get('/logout', (req, res) => {
        req.logout(); // passport
        if (req.session) req.session.destroy();
        res.cookie('user_sid', '', { expires: new Date() });
        // some browsers don't allow redirects after removing cookies (I think)
        res.send('logged out');
    });

    app.get('/error', (req, res) => {
        res.render('error');
    });

    app.use((req, res, next) => {
        res.status(404).render('404');
    });

    app.use((err, req, res, next) => {
        var task = '(err, req, res, next)';
        log.error({ task: task, err: err });
        res.status(500).render('error');
    });

    app.use((err, req, res, next) => {
        res.status(500).render('500');
    });
};