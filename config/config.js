module.exports = {
    database: 'mongodb://opiaapp:V10l3n7Wr1s7s@cluster0-shard-00-00-pqzxv.mongodb.net:27017,cluster0-shard-00-01-pqzxv.mongodb.net:27017,cluster0-shard-00-02-pqzxv.mongodb.net:27017/iPM_Staat?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin',
    models: {
        user: {
            properties: {
                primary: ['email', 'pass'],
                secondary: ['type', 'created_at'],
                _unique: {
                    primary: ['email'],
                    secondary: []
                },
                submittable: ['email', 'pass']
            },
            projections: {
                serialize: ['_id', 'type', 'created_at', 'email'],
                web: ['_id', 'type', 'created_at', 'email'],
                api: ['_id', 'type', 'created_at', 'email']
            }
        },
        map: {
            properties: {
                primary: ['display_name', 'created_by', 'description'],
                secondary: ['type', 'created_at', 'map_date', 'map_office', 'map_state', 'map_basis'],
                _unique: {
                    primary: [],
                    secondary: []
                },
                submittable: ['display_name', 'description', 'map_date', 'map_office', 'map_state', 'map_basis']
            },
            projections: {
                web: ['_id', 'type', 'email', 'display_name', 'created_by', 'created_at', 'description', 'map_date', 'map_office', 'map_state', 'map_basis'],
                api: ['_id', 'type', 'display_name', 'created_by', 'created_at', 'description', 'map_date', 'map_office', 'map_state', 'map_basis']
            }
        }
    },
    secrets: {
        cookies: 'y+BOUCrertcn1HiE2s5dWSOG/U1FXZTCQstS7UpLtG7girssuT8sl5FqGPe4VRioiu7zwnb9MuH9O1uibzM6aNFM1XuW3aZ2equ3pODCwFVpAS++1xVe9HqgCK0zudJxASrIipliOyFkXwZuw1Y6fN6SOAYJoY7tBEw/GVSSOdBexNeHJCRhOBA9VSCudhBOopGmhg+jy9yCb2sTJZfNpYNaaJgh1Qt7CgqcnXXGYpy2mYMNlmkFtls+rQjY9R5InGJRxAvdNA3YbAwfHoND5csP6JbAPmd2BpQ7SeQn4+zKmEKNmJxlQpwsvSRhhCMAl4i6qCEqNYQNJKKHaKGi2Q=='
    }
};