# README #

Um's aufzusetzen:
- git clone
- node installieren
- mit dem Terminal/PowerShell in den Ordner cd'en
- 'npm i' ausführen

Um den WebServer zu starten:
- '[sudo] npm start' || '[sudo] node app.js' || '[sudo] nodemon' ausführen

Um logdaten zu verschönern:
- '[sudo] npm i -g bunyan' ausführen
- '[sudo] npm start | bunyan -o short' ausführen - nicht sicher ob Windows pipes mag